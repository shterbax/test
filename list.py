test_list = [1, 2, 10, 3, 8]
print(test_list[1])

s = test_list[:2]
print(s)

l = len(s)
print(l)

m = max(test_list)
print(m)

d = (1, 4, 3)
print(d)

print(hash(d))

t = test_list.copy()

print(t)

t.append(12)
print(t)

t.reverse()
print(t)

dd = {'1':2, 'test':'test2', 10:24}
print(dd)
# ddd = dd[1] not work
# print(ddd) not work

tj = ['idi', 'na', 'hui']
sep = ' '
print(sep.join(tj))
stj = sep.join(tj)
print(stj.split())

from collections import OrderedDict
st = [1, 10, 1, 5, 1, 3]
test_st = list(OrderedDict.fromkeys(st))
print(test_st)

dl = [1, 4, '12']
a, b, c = dl
n = '\n'
print(a, n,  b, n, c)

print(list(range(10)))
print(tuple(range(10)))