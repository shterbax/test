string_with_bad_spaces = '     123 abc     '
good_string = '123 abc'

# dell first and last symbols
def reverse_and_replace():

    del_end_symbols = string_with_bad_spaces.strip()

    if del_end_symbols == good_string:
        result = 'Krasavchik'
    else:
        result = 'suka blayt idi na hui'
    return result

print(reverse_and_replace() + "\n")


string = 'AB\cd\EF\GM\tk\eo\iv'
cap = ['AB', 'EF', 'GM']
low = ['cd', 'tk', 'eo', 'iv']

# split string
def string_split():
    import json

    string_to_json = json.dumps(string)
    slash_replace_and_split = string_to_json.replace('\\', ' ').replace('"', '').split()
    result_cap = []
    result_low = []

    for x in slash_replace_and_split:
        if x.isupper():
            result_cap.append(x)
        else:
            result_low.append(x)

    print('cap = ' + str(result_cap == cap))
    print('low = ' + str(result_low == low))

string_split()
